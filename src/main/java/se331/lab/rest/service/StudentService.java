package se331.lab.rest.service;

import lombok.extern.slf4j.Slf4j;
import se331.lab.rest.entity.Student;

import java.util.List;

public interface StudentService {
    List<Student> getAllStudent ();
    Student findById(Long id);
    Student saveStudent(Student student);
}
